package com.citi.trading.model;

import org.junit.Before;
import org.junit.Test;
/**
 * Unit test for {@link com.citi.trading.model.Trade}.
 *
 * @author Neueda
 *
 */
public class TradeTests {

    private static final int testId = 0;
    private static final String testStock = "AAPL";
    private static final double testPrice = 99.99;
    private static final int testVolume = 555;
    private Trade testTrade;

    @Before
    public void createTestTrade() {
        testTrade = new Trade(testId, testStock, testPrice, testVolume);
    }

    @Test
    public void test_Trade_fullConstructor() {
        assert (testTrade.getId() == testId);
        assert (testTrade.getStock().equals(testStock));
        assert (testTrade.getPrice() == testPrice);
        assert (testTrade.getVolume() == testVolume);
    }

    @Test
    public void test_Trade_setters() {
        int updatedId = testId + 22;
        String updatedStock = "AMZN";
        double updatedPrice = testPrice + 1111.11;
        int updatedVolume = testVolume + 33;

        testTrade.setId(updatedId);
        testTrade.setStock(updatedStock);
        testTrade.setPrice(updatedPrice);
        testTrade.setVolume(updatedVolume);

        assert (testTrade.getId() == updatedId);
        assert (testTrade.getStock().equals(updatedStock));
        assert (testTrade.getPrice() == updatedPrice);
        assert (testTrade.getVolume() == updatedVolume);
    }

    @Test
    public void test_Trade_toString() {
        assert (testTrade.toString() != null);
        assert (testTrade.toString().contains(testTrade.getStock()));
    }
}
