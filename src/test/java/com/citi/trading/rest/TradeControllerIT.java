package com.citi.trading.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.trading.model.Trade;

/**
 * Integration test for Trade REST Interface.
 *
 * Makes HTTP requests to {@link com.citi.trading.rest.TradeController}.
 *
 * @author Neueda
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2test")
@Transactional
public class TradeControllerIT {

    private static final Logger logger =
                        LoggerFactory.getLogger(TradeControllerIT.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Value("${com.citi.trading.rest.trade-base-path:/trade}")
    private String tradeBasePath;

    @Test
    public void findAll_returnsList() {
        Trade testTrade = new Trade(-1, "NFLX", 22.22, 9999);
        restTemplate.postForEntity(tradeBasePath,
                                   testTrade, Trade.class);

        ResponseEntity<List<Trade>> findAllResponse = restTemplate.exchange(
                                tradeBasePath,
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trade>>(){});

        assertEquals(findAllResponse.getStatusCode(), HttpStatus.OK);
        assertEquals(findAllResponse.getBody().get(0).getStock(), testTrade.getStock());
        assertEquals(findAllResponse.getBody().get(0).getPrice(), testTrade.getPrice(), 0.1);
        assertEquals(findAllResponse.getBody().get(0).getVolume(), testTrade.getVolume());
    }

    @Test
    public void findById_returnsCorrectId() {
        Trade testTrade = new Trade(-1, "NFLX", 22.22, 9999);
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                                           testTrade, Trade.class);

        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);

        Trade foundTrade = restTemplate.getForObject(
                                tradeBasePath + "/" + createdResponse.getBody().getId(),
                                Trade.class);

        assertEquals(foundTrade.getId(), createdResponse.getBody().getId());
        assertEquals(foundTrade.getStock(), testTrade.getStock());
        assertEquals(foundTrade.getPrice(), testTrade.getPrice(), 0.1);
        assertEquals(foundTrade.getVolume(), testTrade.getVolume());
    }

    @Test
    public void deleteById_deletes() {
        Trade testTrade = new Trade(-1, "NFLX", 22.22, 9999);
        ResponseEntity<Trade> createdResponse =
                restTemplate.postForEntity(tradeBasePath,
                                           testTrade, Trade.class);

        assertEquals(createdResponse.getStatusCode(), HttpStatus.CREATED);

        Trade foundTrade = restTemplate.getForObject(
                                tradeBasePath + "/" + createdResponse.getBody().getId(),
                                Trade.class);

        logger.debug("Before delete, findById gives: " + foundTrade);
        assertNotNull(foundTrade);

        restTemplate.delete(tradeBasePath + "/" + createdResponse.getBody().getId());

        ResponseEntity<Trade> response = restTemplate.exchange(
                                tradeBasePath + "/" + createdResponse.getBody().getId(),
                                HttpMethod.GET,
                                null,
                                Trade.class);

        logger.debug("After delete, findById response code is: " +
                     response.getStatusCode());
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }
}
