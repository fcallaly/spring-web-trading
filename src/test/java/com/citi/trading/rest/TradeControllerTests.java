package com.citi.trading.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.trading.model.Trade;
import com.citi.trading.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Unit test for Unit test for {@link com.citi.trading.rest.TradeController}.
 *
 * Uses Spring WebMvc to interface with controller. Uses Mock TradeService.
 *
 * @author Neueda
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = TradeController.class)
public class TradeControllerTests {

    private static final Logger logger =
                LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService mockTradeService;

    @Value("${com.citi.trading.rest.trade-base-path:/trade}")
    private String tradeBasePath;

    @Test
    public void findAllTrades_returnsList() throws Exception {
        when(mockTradeService.findAll())
            .thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc
                .perform(get(tradeBasePath))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber())
                .andReturn();

        logger.info("Result from productService.findAll: "
                    + result.getResponse().getContentAsString());
    }

    @Test
    public void createTrade_returnsCreated() throws Exception {
        Trade testTrade = new Trade(55, "AAPL", 29.99, 7070);

        when(mockTradeService
                .create(any(Trade.class)))
            .thenReturn(testTrade.getId());

        this.mockMvc.perform(
                post(tradeBasePath)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id")
                    .value(testTrade.getId()))
                .andReturn();
        logger.info("Result from Create Trade");
    }

    @Test
    public void findById_returnsOK() throws Exception {
        Trade testTrade = new Trade(55, "NFLX", 9999.00, 3030);

        when(mockTradeService
                .findById(testTrade.getId()))
            .thenReturn(testTrade);

        MvcResult result = this.mockMvc.perform(
                get(tradeBasePath + "/" + testTrade.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTrade)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id")
                        .value(testTrade.getId()))
                .andExpect(jsonPath("$.stock")
                        .value(testTrade.getStock()))
                .andReturn();

        logger.info("Result from Create Trade: " +
                    result.getResponse().getContentAsString());

    }

    @Test
    public void deleteTrade_returnsOK() throws Exception {
        MvcResult result = this.mockMvc
                .perform(delete(tradeBasePath + "/1"))
                .andExpect(status().isNoContent())
                .andReturn();

        logger.info("Result from productService.delete: "
                    + result.getResponse().getContentAsString());
    }
}