package com.citi.trading.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.dao.TradeDao;
import com.citi.trading.model.Trade;

/**
 * Unit test for {@link com.citi.trading.service.TradeService}. Uses mock TradeDao.
 * 
 * @author Neueda
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TradeServiceTests {

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeDao mockTradeDao;

    @Test
    public void test_createRuns() {
        int newId = 1;
 
        Trade testTrade = new Trade(99, "GOOGL", 9.99, 550);
        
        when(mockTradeDao.create(any(Trade.class))).thenReturn(newId);
        int createdId = tradeService.create(testTrade);
        
        verify(mockTradeDao).create(testTrade);
        assertEquals(newId, createdId);
    }

    @Test
    public void test_findById() {
        int testId = 66;

        Trade testTrade = new Trade(testId, "AMZN", 100.99, 250);

        when(mockTradeDao.findById(testId)).thenReturn(testTrade);

        Trade returnedTrade = tradeService.findById(testId);

        verify(mockTradeDao).findById(testId);
        assertEquals(testTrade, returnedTrade);
    }

    @Test
    public void test_findAll() {
        List<Trade> testList = new ArrayList<Trade>();
        testList.add(new Trade(33, "AAPL", 500.99, 3050));

        when(mockTradeDao.findAll()).thenReturn(testList);
        List<Trade> returnedList = tradeService.findAll();
        
        verify(mockTradeDao).findAll();
        assertEquals(testList, returnedList);
    }

    @Test
    public void test_deleteById() {
        tradeService.deleteById(5);

        verify(mockTradeDao).deleteById(5);
    }
}
