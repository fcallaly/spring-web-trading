package com.citi.trading.dao.mysql;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.trading.exceptions.TradeNotFoundException;
import com.citi.trading.model.Trade;

/**
 * Unit test for {@link com.citi.trading.dao.mysql.MysqlTradeDao}.
 *
 * Uses the in-memory h2 database.
 *
 * @author Neueda
 *
 */
@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradeDaoTests {

    @SpyBean
    private JdbcTemplate tpl;

    @Autowired
    private MysqlTradeDao mysqlTradeDao;

    private Trade testTrade;
    private int testId = -1;
    private String testStock = "AMZN";
    private double testPrice = 55.55;
    private int testVolume = 99999;

    /**
     * Runs before each test, create a Trade object for testing.
     */
    @Before
    public void createTestTrade() {
        testTrade = new Trade(testId, testStock,
                              testPrice, testVolume);
    }

    @Test
    public void test_createAndFindAll_works() {
        mysqlTradeDao.create(new Trade(1, "GOOGL", 99.99, 100));
        assertTrue(mysqlTradeDao.findAll().size() >= 1);
    }

    @Test
    public void test_createAndFindById_works() {
        int newId = mysqlTradeDao.create(testTrade);
        assertNotNull(mysqlTradeDao.findById(newId));
    }

    @Test(expected = TradeNotFoundException.class)
    public void test_createAndFindById_returnsNotFound() {
        mysqlTradeDao.create(testTrade);
        mysqlTradeDao.findById(4444);
    }

    @Test
    public void test_createAndDeleteById_works() {
        int tradeId = mysqlTradeDao.create(testTrade);
        mysqlTradeDao.deleteById(tradeId);
    }

    @Test(expected = TradeNotFoundException.class)
    public void test_createAndDeleteById_notFound() {
        mysqlTradeDao.create(testTrade);
        mysqlTradeDao.deleteById(777);
    }
}
