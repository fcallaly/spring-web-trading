package com.citi.trading.rest;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.citi.trading.exceptions.TradeNotFoundException;

/**
 * Exception handler applied to all REST Controllers.
 *
 * @author Neueda
 *
 */
@ControllerAdvice
@Priority(20)
public class DefaultExceptionHandler {

    private static final Logger logger =
            LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(value = {TradeNotFoundException.class})
    public ResponseEntity<Object> productNotFoundExceptionHandler(
            HttpServletRequest request, TradeNotFoundException ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
}