package com.citi.trading.exceptions;

/**
 * An exception to be thrown by {@link com.citi.trading.dao.TradeDao} implementations
 * when a requested trade is not found.
 *
 * @author Neueda
 *
 */
@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {
    public TradeNotFoundException(String msg) {
        super(msg);
    }
}
