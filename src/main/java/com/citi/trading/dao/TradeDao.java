package com.citi.trading.dao;

import java.util.List;

import com.citi.trading.model.Trade;

/**
 * 
 * Generic DAO Interface for Trade model.
 * 
 * @author Neueda
 *
 */
public interface TradeDao {

    List<Trade> findAll();

    int create(Trade Trade);

    Trade findById(int id);

    void deleteById(int id);
}
