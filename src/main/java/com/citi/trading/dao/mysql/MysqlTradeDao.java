package com.citi.trading.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.trading.dao.TradeDao;
import com.citi.trading.exceptions.TradeNotFoundException;
import com.citi.trading.model.Trade;

/**
 * JDBC MySQL DAO implementation for trade table.
 * 
 * @author Neueda
 *
 */
@Component
public class MysqlTradeDao implements TradeDao {

    private static final Logger logger =
                            LoggerFactory.getLogger(MysqlTradeDao.class);

    private static String FIND_ALL_SQL = "select id, stock, price, volume from trade";
    private static String FIND_SQL = "select id, stock, price, volume from trade where id = ?";
    private static String INSERT_SQL = "insert into trade (stock, price, volume) values (?, ?, ?)";
    private static String DELETE_SQL = "delete from trade where id=?";

    @Autowired
    private JdbcTemplate tpl;

    public List<Trade> findAll(){
        logger.debug("findAll SQL: [" + FIND_ALL_SQL + "]");
        return tpl.query(FIND_ALL_SQL,
                         new TradeMapper());
    }

    @Override
    public Trade findById(int id) {
        logger.debug("findBydId(" + id + ") SQL: [" + FIND_SQL + "]");
        List<Trade> Trades = this.tpl.query(FIND_SQL,
                new Object[]{id},
                new TradeMapper()
        );
        if(Trades.size() <= 0) {
            String warnMsg = "Requested Trade not found, id: " + id;
            logger.warn(warnMsg);
            throw new TradeNotFoundException(warnMsg);
        }
        if(Trades.size() > 1) {
            logger.warn("Found more than one Trade with id: " + id);
        }
        return Trades.get(0);
    }

    @Override
    public int create(Trade trade) {
        logger.debug("create( " + trade + ") SQL: [" + INSERT_SQL + "]");
        KeyHolder keyHolder = new GeneratedKeyHolder();
        this.tpl.update(
            new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps =
                            connection.prepareStatement(INSERT_SQL,
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, trade.getStock());
                    ps.setDouble(2, trade.getPrice());
                    ps.setInt(3,  trade.getVolume());
                    return ps;
                }
            },
            keyHolder);
        logger.debug("Created Trade with id: " + keyHolder.getKey().intValue());
        return keyHolder.getKey().intValue();
    }

    @Override
    public void deleteById(int id) {
        logger.debug("deleteById(" + id + ") SQL: [" + DELETE_SQL +"]");
        if(this.tpl.update(DELETE_SQL, id) <= 0) {
            String warnMsg = "Failed to delete, trade not found: " + id;
            logger.warn(warnMsg);
            throw new TradeNotFoundException(warnMsg);
        }
    }

    /**
     * private internal class to map database rows to Trade objects.
     *
     */
    private static final class TradeMapper implements RowMapper<Trade> {
        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
            logger.debug("Mapping trade result set row num [" + rowNum + "], id : [" +
                         rs.getInt("id") + "]");
        
            return new Trade(rs.getInt("id"),
                             rs.getString("stock"),
                             rs.getDouble("price"),
                             rs.getInt("volume"));
        }
    }
}
