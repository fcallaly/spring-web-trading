package com.citi.trading.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.trading.dao.TradeDao;
import com.citi.trading.model.Trade;

/**
 * Service layer class for basic {@link com.citi.trading.Trade} resource operations.
 * 
 * @author Neueda
 *
 */
@Component
public class TradeService {

    @Autowired
    private TradeDao tradeDao;

    public List<Trade> findAll() {
        return tradeDao.findAll();
    }

    public int create(Trade Trade) {
        return tradeDao.create(Trade);
    }

    public Trade findById(int id) {
        return tradeDao.findById(id);
    }

    public void deleteById(int id) {
        tradeDao.deleteById(id);
    }
}
