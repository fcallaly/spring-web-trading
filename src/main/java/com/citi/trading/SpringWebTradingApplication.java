package com.citi.trading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebTradingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebTradingApplication.class, args);
    }
}
